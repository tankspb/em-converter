﻿### http://soundfile.sapp.org/doc/WaveFormat/
### http://microsin.net/programming/pc/wav-format.html

function Update-IntInArrayOfBytes {
    param (
        [parameter (mandatory=$true)][byte[]]$bytes,
        [parameter (mandatory=$true)][uint32]$startAddress,
        [parameter (mandatory=$true)][uint32]$intValue
    )

    $hexString = [String]::Format("{0:x8}", $intValue) # 98 24 dc 67
    for ($i = 7; $i -ge 0; $i-=2) {
        $bytes[$startAddress] = ([Convert]::ToString('0x'+$hexString.Substring(($i-1),2), 10) ) -as [byte]
        $startAddress += 1
    }

    return $bytes
}

# Read header from wav file. Bytes 0..43 
function Read-WavHeader {
    param (
        [parameter (mandatory=$true)][byte[]]$bytes
    )

    $wavHeader = New-Object -TypeName psobject
    $wavHeader | Add-Member -MemberType NoteProperty -Name riff         -Value ([System.Text.Encoding]::ASCII.getString($bytes[0..3]) )
    $wavHeader | Add-Member -MemberType NoteProperty -Name filesize     -Value ([convert]::touint32( ($bytes[07..04] | foreach Tostring X2) -join '', 16))
    $wavHeader | Add-Member -MemberType NoteProperty -Name wavefmt      -Value ([System.Text.Encoding]::ASCII.getString($bytes[08..15]) )
    $wavHeader | Add-Member -MemberType NoteProperty -Name wavefmtsize  -Value ([convert]::touint32( ($bytes[19..16] | foreach Tostring X2) -join '', 16))
    $wavHeader | Add-Member -MemberType NoteProperty -Name audioformat  -Value ([convert]::touint32( ($bytes[21..20] | foreach Tostring X2) -join '', 16))
    $wavHeader | Add-Member -MemberType NoteProperty -Name channels     -Value ([convert]::touint32( ($bytes[23..22] | foreach Tostring X2) -join '', 16))
    $wavHeader | Add-Member -MemberType NoteProperty -Name sampleRate   -Value ([convert]::touint32( ($bytes[27..24] | foreach Tostring X2) -join '', 16))
    $wavHeader | Add-Member -MemberType NoteProperty -Name byteRate     -Value ([convert]::touint32( ($bytes[31..28] | foreach Tostring X2) -join '', 16))
    $wavHeader | Add-Member -MemberType NoteProperty -Name blockAlign   -Value ([convert]::touint32( ($bytes[33..32] | foreach Tostring X2) -join '', 16))
    $wavHeader | Add-Member -MemberType NoteProperty -Name bitPerSample -Value ([convert]::touint32( ($bytes[35..34] | foreach Tostring X2) -join '', 16))
    $wavHeader | Add-Member -MemberType NoteProperty -Name data         -Value ([System.Text.Encoding]::ASCII.getString($bytes[36..39]) )
    $wavHeader | Add-Member -MemberType NoteProperty -Name datasize     -Value ([convert]::touint32( ($bytes[43..40] | foreach Tostring X2) -join '', 16))

    $wavHeader | Add-Member -MemberType NoteProperty -Name datasizereal -Value ($wavHeader.datasize)
    if ($wavHeader.datasize -gt ($wavHeader.filesize-36) ) {
        $wavHeader.datasizereal = $wavHeader.filesize-36
    }

    return $wavHeader
}

#$wavpath = "F:\Tank\OP-Sound\elmod-sound-converter\samples\"
$wavpath = "F:\Tank\OP-Sound\em-converted\em_t-34-85_v1.0\"
#$wavfile = "motcold1-1"
$wavfile = ""
#$wavmatch = '.*-a\.wav'
#$wavmatch = 'motcold1-(b|c)\.wav'
$wavmatch = '(mot(.*)|turn)\.wav'
$wavList = Get-ChildItem $wavpath | where {$_.name -match $wavmatch } | foreach {$_.name -replace('.wav','') }
if (($wavfile -ne $null) -and ($wavfile -ne '')) { $wavList = $wavfile }


foreach ($wavfile in $wavList) {

    $fullfilename = $wavpath+$wavfile+'.wav'

    Write-Output "Process file: $fullfilename"

    # Read the entire file to an array of bytes.
    $wavDataIn = [System.IO.File]::ReadAllBytes($fullfilename)

    # Read header of wav file
    $wavHeader = Read-WavHeader -bytes $wavDataIn[0..43]
    $wavHeaderOut = Read-WavHeader -bytes $wavDataIn[0..43]

    # Save wav header in new array 
    $wavDataOut = $wavDataIn[0..43]

    # Process wav data
    # 8-bit  : Noise: 16 samples, 16 bytes. Repeats every 512 samples 
    # 16-bit : Noise: 16 samples, 32 bytes. Repeats every 1024 samples 
    #$noiseSampleSize = 32
    $noiseSampleSize = 16*$wavHeader.blockAlign
    $noiseInterval   = 512*$wavHeader.blockAlign
    switch ($wavHeader.blockAlign) {
        1 {$noiseStartAddr  = 512}
        2 {$noiseStartAddr  = 980}
    }

    $wavDataOut += $wavDataIn[(44+$noiseSampleSize)..($noiseStartAddr-1)]
    #$wavHeaderOut.datasize = $noiseStartAddr-$noiseSampleSize

    for ($i=$noiseStartAddr; $i -le $wavHeader.datasizereal -$noiseStartAddr; $i += $noiseInterval ){
        switch ($wavHeader.blockAlign) {
            1 { $current = [convert]::tosbyte( ( ($wavDataIn[$i] |  foreach Tostring X2)  -join '') ,16) } 
            2 { $current = [convert]::toint16( ( ($wavDataIn[($i+1)..($i)] |  foreach Tostring X2)  -join '') ,16) } 
        } 

        Write-Output "Pos: $i; Value: $current; $compareresult "

        $wavDataOut += $wavDataIn[($i+$noiseSampleSize)..($i+$noiseInterval-1)]
    }

    $wavHeaderOut.datasize = $wavDataOut.Count - 44
    $wavHeaderOut.filesize = $wavDataOut.Count - 8

    $wavDataOut = Update-IntInArrayOfBytes -bytes $wavDataOut -startAddress 04 -intValue $wavHeaderOut.filesize 
    $wavDataOut = Update-IntInArrayOfBytes -bytes $wavDataOut -startAddress 40 -intValue $wavHeaderOut.datasize 


    Write-Output ("Save: "+$wavpath+$wavfile+'-converted.wav')
    [io.file]::WriteAllBytes($wavpath+$wavfile+'-converted.wav',$wavDataOut)

}

<#

$current = 0
$currnoise = 0;
$startNoise = 980
$stepNoise =1024
#for ($i=$startNoise; $i -le 1000; $i+=2 ){
#for ($i=337860; $i -le 337876+100; $i+=2 ){
for ($i=$startNoise; $i -le $datasize-$startNoise; $i+=$stepNoise ){
#for ($i=44; $i -le 500; $i+=2 ){
#    $i 
    $prev = $current
    $current = [convert]::toint16( ( ($bytes[($i+1)..($i)] |  foreach Tostring X2)  -join '') ,16)
    if ([math]::abs($current) -le 16000) { $compareresult = " fail"}
    Write-Output "Pos: $i; Value: $current; $compareresult "
    $compareresult = ""
}
#>


<#

[byte[]]$bytesIn  = (1,2,3,4,5,6,7,8,9,0,11,12,13,14,15,16,17,18,19,20)
[byte[]]$bytesOut = @()

$bytesOut = $bytesIn[0..6]
$bytesOut += $bytesIn[10..13]
$bytesOut += $bytesIn[15..16]


$bytesOut = Update-IntInArrayOfBytes -bytes $bytesOut -startAddress 3 -intValue 2552552551

$bytesIn
Write-Output "!!!"
$bytesOut
#>



###### Archive
<#
# Read the entire file to an array of bytes.
$bytes = [System.IO.File]::ReadAllBytes("C:\Temp\motcold1-2.wav")
#RIFF              $bytes[04..00] 
$wavsize = [convert]::touint32( ($bytes[07..04] | foreach Tostring X2) -join '', 16)  
#"WAVEfmt "        $bytes[15..08] 
$fmtsize = [convert]::touint32( ($bytes[19..16] | foreach Tostring X2) -join '', 16)  
$aformat = [convert]::touint32( ($bytes[21..20] | foreach Tostring X2) -join '', 16)  
$channel = [convert]::touint32( ($bytes[23..22] | foreach Tostring X2) -join '', 16)  
$smprate = [convert]::touint32( ($bytes[27..24] | foreach Tostring X2) -join '', 16)  
$bytrate = [convert]::touint32( ($bytes[31..28] | foreach Tostring X2) -join '', 16)  
$blockal = [convert]::touint32( ($bytes[33..32] | foreach Tostring X2) -join '', 16)  
$btpersa = [convert]::touint32( ($bytes[35..34] | foreach Tostring X2) -join '', 16)  
#"data"                         $bytes[39..36] 
$datasize = [convert]::touint32( ($bytes[43..40] | foreach Tostring X2) -join '', 16)  
# ..44
#>